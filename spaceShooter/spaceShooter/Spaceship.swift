//
//  SpaceShip.swift
//  spaceShooter
//
//  Created by Johan van der Meulen on 16/09/15.
//  Copyright (c) 2015 Johan van der Meulen. All rights reserved.
//

import SpriteKit

class Spaceship {
    let _sprite: SKSpriteNode?
    let _xScale: Float?
    let _yScale: Float?
    let _location: UITouch?
    let _view: SKScene?
    
    
    init(sprite: SKSpriteNode, xScale: Float, yScale: Float, location: UITouch, view: SKScene)
    {
        _sprite = sprite
        _xScale = xScale
        _yScale = yScale
        _location = location
        _view = view
        
        println("A spaceship has been born")
    }
    
    
    
}
