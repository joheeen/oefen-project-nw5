//
//  Instantiable.swift
//  spaceShooter
//
//  Created by Johan van der Meulen on 16/09/15.
//  Copyright (c) 2015 Johan van der Meulen. All rights reserved.
//

import SpriteKit

class Instantiable {
    var _sprite: SKSpriteNode? {
        set {}
        get {
            return self._sprite
        }
    }
    var _xScale: Float? {
        set {}
        get {
            return self._xScale
        }
    }
    var _yScale: Float? {
        set {}
        get {
            return self._yScale
        }
    }
    var _location: UITouch? {
        set {}
        get {
            return self._location
        }
    }
    var _view: SKScene? {
        set {}
        get {
            return self._view
        }
    }
    
    
    init(sprite: SKSpriteNode, xScale: Float, yScale: Float, location: UITouch, view: SKScene, debugText: String?)
    {
        _sprite = sprite
        _xScale = xScale
        _yScale = yScale
        _location = location
        _view = view
        
        if let debugText = debugText as String! {
            println(debugText)
        }
    }

}